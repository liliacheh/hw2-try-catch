const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  const div = document.getElementById('root');
  div.insertAdjacentHTML('afterbegin',`<ul class="list">Books</ul>`);
  const list = document.querySelector('.list');
  list.style.listStyleType = 'none';
  const props = ['author', 'name', 'price'];

  function checkList(arr, needProps){
    let finalArr = [];
    arr.forEach((item, index) =>{
try{
    needProps.forEach(prop => {
        if (!(prop in item)) {
            throw new Error(`Book #${index + 1} does not have '${prop}'`)
        }
    })
    finalArr.push(item);
} catch (err) {
    console.error(err.name + ': ' + err.message);
}
})
    return finalArr;
}

function addItem(arr){
 arr.map(item => {
    return Object.entries(item).map(([key, value]) => {
        return list.insertAdjacentHTML('beforeend', `<li>${key}: ${value}</li>`)
    })
 })
}

addItem(checkList(books, props));
